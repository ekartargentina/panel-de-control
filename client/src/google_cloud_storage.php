<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require("../vendor/autoload.php");

use Google\Cloud\Storage\StorageClient;

putenv('GOOGLE_APPLICATION_CREDENTIALS=C:/GAI/claves/clave-e-kart-webradio-google-apps/panelcontrol/baltazar-leguisamon-02c73d79ee8a.json');

# Includes the autoloader for libraries installed with composer
// require __DIR__ . '/vendor/autoload.php';

# Imports the Google Cloud client library
// use Google\Cloud\Storage\StorageClient;

# Your Google Cloud Platform project ID
$projectId = 'baltazar-leguisamon';

# Instantiates a client
$storage = new StorageClient([
    'projectId' => $projectId
]);

# The name for the new bucket
// $bucketName = 'my-new-bucket';

# Creates the new bucket
// $bucket = $storage->createBucket($bucketName);

// echo 'Bucket ' . $bucket->name() . ' created.';


/**
 * List all Cloud Storage buckets for the current project.
 *
 * @return void
 */
function list_buckets()
{
	$projectId = 'baltazar-leguisamon';
    $storage = new StorageClient([
	    'projectId' => $projectId
	]);
    foreach ($storage->buckets() as $bucket) {
        printf('Bucket: %s' . PHP_EOL, $bucket->name());
    }
}



/**
 * List Cloud Storage bucket objects.
 *
 * @param string $bucketName the name of your Cloud Storage bucket.
 *
 * @return void
 */
function list_objects($bucketName)
{
    $storage = new StorageClient();
    $bucket = $storage->bucket($bucketName);
    foreach ($bucket->objects() as $object) {
    	echo "<strong>Object: ".$object->name()."</strong><br>";
        // printf('Object: %s' . PHP_EOL, $object->name());
    }
}


/**
 * List Cloud Storage bucket objects with specified prefix.
 *
 * @param string $bucketName the name of your Cloud Storage bucket.
 *
 * @return void
 */
function list_objects_with_prefix($bucketName, $prefix)
{
	$list = [];
    $storage = new StorageClient();
    $bucket = $storage->bucket($bucketName);
    $options = ['prefix' => $prefix];
    foreach ($bucket->objects($options) as $object) {
    	// echo 'Object: '. $object->name() . '<br>';
    	array_push($list, $object->name());
    }

    return $list;
}


/**
 * Download an object from Cloud Storage and save it as a local file.
 *
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of your Google Cloud object.
 * @param string $destination the local destination to save the encrypted object.
 *
 * @return void
 */
function download_object($bucketName, $objectName, $destination)
{
    $storage = new StorageClient();
    $bucket = $storage->bucket($bucketName);
    $object = $bucket->object($objectName);
    $object->downloadToFile($destination);

    // printf('Downloaded gs://%s/%s to %s' . PHP_EOL,
        // $bucketName, $objectName, basename($destination));

    header("Content-type: text/csv");
    $my_file = $destination;
	$handle = fopen($my_file, 'r');
	$data = fread($handle,filesize($my_file));
	echo $data;
}


$reportTypes = [
				"APP_VERSION"=>"app_version", 
				"CARRIER"=>"carrier",
				"COUNTRY"=>"country", 
				"DEVICE"=>"device", 
				"LANGUAGE"=>"language", 
				"OS_VERSION"=>"os_version", 
				"OVERVIEW"=>"overview"
			];

$bucketName = 'pubsite_prod_rev_09284810666443127763';
// $objectName = 'stats/installs/installs_com.grupoargentinoinformatico.xvisael_201806_app_version.csv';
$destination = 'C:\wamp64\www\ekart-webradio\webradio-admin\reports\\';
$app = 'webradio.ekart.com.ar';

$anio = '2019';
$mes = '05';

// Todos los reportes
$reportType = "";

//Reporte de paises
$reportType = $reportTypes["COUNTRY"];

$prefix = 'stats/installs/installs_'.$app."_".$anio."".$mes."_".$reportType;

// list_buckets();
// list_objects($bucketName);
$reports = list_objects_with_prefix($bucketName, $prefix);
// var_dump($reports);

download_object($bucketName, $reports[0], $destination."".$reports[0]);


















?>