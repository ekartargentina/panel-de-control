import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

	firebaseUrl = "https://fcm.googleapis.com/fcm/send";

  constructor( private http: HttpClient ) { }

  send(bodyData): Observable<any[]>
  {
  	const httpOptions = {
		  headers: new HttpHeaders({ 
		  	'Content-Type': 'application/json',
		  	'Authorization':'key=AAAAhygL5Mg:APA91bFU2TekeEdEI8oiDhWalBTtnKvlC2-iYn3Ub-n9bC2IY_A5wGsc2R5ZXianpKq5ucflR9B70W6WtUXWPluLDMdZ57zdpEY081C3DhiIqA7bdVrCf6jwDsJnjJSLmsnRPfPnMWEpf97kGbYcoiGDfK6YUPtowg',
		  })
		};

  	return this.http.post<any>(this.firebaseUrl, bodyData, httpOptions);
	  
  }

}
