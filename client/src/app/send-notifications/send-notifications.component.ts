import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { FirebaseService } from '../firebase.service';

import { catchError, map, tap } from 'rxjs/operators';


@Component({
  selector: 'app-send-notifications',
  templateUrl: './send-notifications.component.html',
  styleUrls: ['./send-notifications.component.sass']
})
export class SendNotificationsComponent implements OnInit {

	textMessage: string;
	titleMessage: string;

  constructor(private toastr: ToastrService,
  	private firebaseService: FirebaseService) { }

  ngOnInit() {
  }

  send(){
  	var bodyData = 
  	{
	     "to" : "/topics/all",
	     "collapse_key" : "type_a",
	     "notification" : {
	         "body" : this.textMessage,
	         "title": this.titleMessage
	     },
	     "data" : {
	         "body" : this.textMessage,
	         "title": this.titleMessage
	    }
	  };

	  this.firebaseService.send(bodyData)
      .subscribe((res:any) => {
      	console.log(res);
			  this.notificacion();
      });

  }

  notificacion()
  {
  	this.toastr.success(this.titleMessage +"\n"+ this.textMessage, '¡Notificación enviada!');
  }
}
/*

<script type="text/javascript">
$(document).ready(function() {
  $(document).on('click', '#enviarMensaje', function(event) {
    var bodyData = {
                   "to" : "/topics/all",
                   "collapse_key" : "type_a",
                   "notification" : {
                       "body" : $("#textMessage").val(),
                       "title": $("#titleMessage").val()
                   },
                   "data" : {
                       "body" : $("#textMessage").val(),
                       "title": $("#titleMessage").val()
                   }
                  };

    $.ajax({
      url: 'https://fcm.googleapis.com/fcm/send',
      headers: {
        'Authorization':'key=AAAAhygL5Mg:APA91bFU2TekeEdEI8oiDhWalBTtnKvlC2-iYn3Ub-n9bC2IY_A5wGsc2R5ZXianpKq5ucflR9B70W6WtUXWPluLDMdZ57zdpEY081C3DhiIqA7bdVrCf6jwDsJnjJSLmsnRPfPnMWEpf97kGbYcoiGDfK6YUPtowg',
      },
      type: 'POST',
      contentType: 'application/json; charset=utf-8',
      data: JSON.stringify(bodyData),
      dataType: "json",
      cache: false,
      success: function(result) {}
    });
  });
});
</script>
*/