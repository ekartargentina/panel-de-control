import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListmountsComponent } from './listmounts.component';

describe('ListmountsComponent', () => {
  let component: ListmountsComponent;
  let fixture: ComponentFixture<ListmountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListmountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListmountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
