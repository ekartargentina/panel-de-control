import { Component, OnInit } from '@angular/core';
import { ListenersService } from '../listeners.service';

@Component({
  selector: 'app-listmounts',
  templateUrl: './listmounts.component.html',
  styleUrls: ['./listmounts.component.sass']
})
export class ListmountsComponent implements OnInit {

	radios:any;
	listmounts:any;

  constructor(private listenersService: ListenersService) { }

  ngOnInit() {
  	this.getStatus();
  }

  getStatus(): void {
  	this.listenersService.status()
  	.subscribe(radios => {
      if(typeof radios.icestats.source !== "undefined"){
    		if(typeof radios.icestats.source[0] === "undefined")
    		{
    			this.listmounts = [radios.icestats.source];
    		}
    		else
    		{
    			this.listmounts = radios.icestats.source;
    		}
      }
  	});
  }

}
