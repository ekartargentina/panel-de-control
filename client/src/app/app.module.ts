import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

// Toast
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

// Modules project
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListmountsComponent } from './listmounts/listmounts.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ListenersComponent } from './listeners/listeners.component';
import { MapListenersComponent } from './map-listeners/map-listeners.component';
import { SendNotificationsComponent } from './send-notifications/send-notifications.component';



import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    ListmountsComponent,
    SidebarComponent,
    ListenersComponent,
    MapListenersComponent,
    SendNotificationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
