import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapListenersComponent } from './map-listeners.component';

describe('MapListenersComponent', () => {
  let component: MapListenersComponent;
  let fixture: ComponentFixture<MapListenersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapListenersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapListenersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
