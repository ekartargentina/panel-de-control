import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
// declare module 'googlemaps';
import {} from 'googlemaps';


@Component({
  selector: 'app-map-listeners',
  templateUrl: './map-listeners.component.html',
  styleUrls: ['./map-listeners.component.sass']
})
export class MapListenersComponent implements OnInit {

  map: google.maps.Map;
	@ViewChild('map')
	mapElement: any;

  constructor() { }

  ngOnInit() {
  	var latlong = new google.maps.LatLng(35.2271, -80.8431);
		const mapProperties = {
        center: latlong,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
   };

   this.map = new google.maps.Map(this.mapElement.nativeElement,    mapProperties);
   var marker = new google.maps.Marker({position: latlong, map: this.map});
  }

}
