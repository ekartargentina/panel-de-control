import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListenersService {

  constructor( private http: HttpClient) { }

  get_radio_listeners(mount): Observable<any>
  {
  	const url = environment.urlAPIRest + "/get_radio_listeners.php?mount="+mount;
  	var user = 'admin';
		var password = 'pepe99';
  	const httpOptions = {
		  headers: new HttpHeaders({
		  	'Content-Type': 'text/xml'
		    // 'Authorization': "Basic "+btoa(user+":"+password)
		  })
		};
  	return this.http.get(url);
  }


  status(): Observable<any>
  {
  	const url = "http://35.185.74.33:3389/status-json.xsl";

  	return this.http.get<any>(url);
  }

  listmounts()
  {
  	const url = environment.urlAPIRest + "/get_radio_listmounts.php";
  	let headers = new HttpHeaders({
		    // 'Content-Type':  'text/xml',
		    // 'Authorization': 'my-auth-token'
		  });

  	return this.http.get(url);
  }


  obtenerUbicacion(ip)
  {
    let url = "http://api.ipstack.com/"+ip+"?access_key=1b61986851b7516d3b8721c49a260e65"
    return this.http.get(url);
  }
}
