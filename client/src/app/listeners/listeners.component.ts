import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { ListenersService } from '../listeners.service';
import { ViewChild } from '@angular/core';
import {} from 'googlemaps';


@Component({
  selector: 'app-listeners',
  templateUrl: './listeners.component.html',
  styleUrls: ['./listeners.component.sass']
})
export class ListenersComponent implements OnInit {

	listeners:string;

	table_listeners:any;

  mounts:any;

  map: google.maps.Map;
  latlong: google.maps.LatLng;
  mapProperties:any;
  boundsMap:google.maps.LatLngBounds;


  intervalMapElement;

 @ViewChild("mapElement", {read: ElementRef}) mapElement: ElementRef;

  constructor(
    private listenersService: ListenersService,
    private renderer: Renderer2) { }

  ngOnInit() {
    this.latlong = new google.maps.LatLng(-34.0000000, -64.0000000);
    this.mapProperties  = {
            center: this.latlong,
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
       };
    this.boundsMap = new google.maps.LatLngBounds();

    this.getMounts();
  }

  ngAfterViewInit() {
    var _self = this;
    _self.intervalMapElement = setInterval(function(){ _self.setMap }, 1000);
  }

  setMap()
  {
    if(typeof this.mapElement.nativeElement !== 'undefined')
    {
      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapProperties);
      clearInterval(this.intervalMapElement);
    }
  }

  getMounts(): void {
    this.listenersService.listmounts()
    .subscribe((listmounts:any) => {
      this.mounts = [];
      let sources = listmounts.source;

      if(typeof sources !== 'undefined' && sources.length>0)
      {
        for (var i of sources) {
          let mount = i['@attributes'].mount;
          this.mounts.push({'mount': mount, 'listeners':[]});
          this.getListenersMount(mount);
        }
      }
      else{
        let mount = listmounts.source['@attributes'].mount;
        this.mounts.push({'mount': mount, 'listeners':[]});
        this.getListenersMount(mount);
      }

    });
  }


  getListenersMount(mount)
  {
    this.listenersService.get_radio_listeners(mount)
    .subscribe(listeners => {
      // console.log(listeners.source);

      // El mount donde se agregan los listeners
      var actualMount = this.mounts.filter(obj => {
        return obj.mount === mount
      });
      actualMount = actualMount[0];

      this.table_listeners = [];
      let list_listeners = listeners.source.listener;
      console.log(listeners.source.Listeners)
      if(typeof listeners.source.Listeners !== 'undefined' && parseInt(listeners.source.Listeners)>0)
      {
        console.log(list_listeners);
        let ip = null;
        let connected = null;
        let location = null;
        if(Array.isArray(list_listeners)){
          for (var i of list_listeners) {
            ip = i.IP;
            connected = i.Connected;
            this.getUbicacionListener(actualMount, ip, connected);
          }
        }else{
          ip = list_listeners.IP;
          connected = list_listeners.Connected;
          this.getUbicacionListener(actualMount, ip, connected);
        }
      }
    });
  }

  getUbicacionListener(actualMount, ip, connected)
  {
    if(!this.isLocationSaved(ip))
    {
      this.listenersService.obtenerUbicacion(ip)
      .subscribe((res:any) => {
        // Obtener respuesta

        //Armar item para guardar en localStorage
        let location = {
          'city': res.city,
          'region': res.region_name,
          'country': res.country_name,
          'lat': res.latitude,
          'long': res.longitude
        };

        var latlong = new google.maps.LatLng(location.lat, location.long);
        var marker = new google.maps.Marker({position: latlong, map: this.map});

        // Agrego el listener a la lista
        var listenerObj = {'ip': ip, 'connected': connected, 'location': location, 'marker': marker, 'position': latlong};
        this.addListenerToMount(listenerObj, actualMount)
  
        // Acomodo el mapa con el nuevo marker
        // this.mapFitBounds(listenerObj);

        // Guardo el location para esta ip en localStorage
        let key = res.ip;
        localStorage.setItem(key, JSON.stringify(location));
      });
    }
    else{
      console.log("Está guardada y lo trae del storage");
      if(typeof this.map === 'undefined')
      {
        console.log("No está sentenciada, lo seteo");
        this.setMap();
      }

      let location = this.getLocationByIp(ip);
      
      var latlong = new google.maps.LatLng(location.lat, location.long);
      
      var marker = new google.maps.Marker({position: latlong, map: this.map});

      var listenerObj = {'ip': ip, 'connected': connected, 'location': location, 'marker': marker, 'position': latlong};
      
      this.addListenerToMount(listenerObj, actualMount);

      // Acomodo el mapa con el nuevo marker
      // this.mapFitBounds(listenerObj);
    }

  }

  isLocationSaved(ip)
  {
    return this.getLocationByIp(ip) != null;
  }

  getLocationByIp(ip)
  {
    return JSON.parse(localStorage.getItem(ip));
  }

  addListenerToMount(listenerObj, actualMount)
  {
    var ip = listenerObj.ip;
    var connected = listenerObj.connected;
    var location = listenerObj.location;
    var marker = listenerObj.marker;
    var position = listenerObj.position;

    actualMount.listeners.push(
      {
        'ip': ip, 
        'connected': connected, 
        'location': location, 
        'marker': marker, 
        'position': position
      });
  }

  mapFitBounds(listenerObj){
    this.boundsMap.extend(listenerObj.position);
    this.map.fitBounds(this.boundsMap);
  }
}
