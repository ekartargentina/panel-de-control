Setting up SSH access to Bitbucket on Windows with PuttyGen
Setting up SSH access to Bitbucket on Windows with PuttyGen

Setting up SSH access to my Bitbucket Mercurial repository on Windows took a bit longer than I would have liked. Since Google doesn’t come up with any direct hits that give a step-by-step guide to exactly dealing with this issue, and since the Bitbucket documentation leaves the Windows user high and dry with a ‘This is not described in further detail here’ message, I’m going to go ahead and write one of my own.

If you don’t have a public/private encryption key pair, you first need to get those:

Download the Putty tools: Make sure you get at least PuttyGen and Pageant. They’re both included in the ‘Windows Installer for Everything…’
Run PuttyGen and click on Generate to create a new key
Setting up SSH access to Bitbucket on Windows with PuttyGen

You can change the ‘Key comment’ to anything you like. Something that includes the encryption method, date, username and computer is probably helpful e.g. rsa-key-20101130-user@home
The ‘Key passphrase’ is something that will be used to secure the private key that’s stored locally on your computer. Unless someone has the passphrase, they cannot access your private key. The PuttyGen documentation says
Choosing a good passphrase is difficult. Just as you shouldn’t use a dictionary word as a password because it’s easy for an attacker to run through a whole dictionary, you should not use a song lyric, quotation or other well-known sentence as a passphrase. DiceWare recommends using at least five words each generated randomly by rolling five dice, which gives over 2^64 possible passphrases and is probably not a bad scheme. If you want your passphrase to make grammatical sense, this cuts down the possibilities a lot and you should use a longer one as a result.

Now you can click on the ‘Save private key’ and ‘Save public key’ buttons to save the files to disk. Don’t close the dialog box yet as we’ll need it soon.

Now go to your Bitbucket home, and click on the Accounts button

Setting up SSH access to Bitbucket on Windows with PuttyGen

Scroll down to the SSH Keys section. Copy your public key from the PuttyGen window where it says ‘Public key for pasting into OpenSSH authorized_keys file’ and paste it in the textbox next to the ‘Add Key’ button. Now press ‘Add Key’ and Bitbucket should store the key for you.

To avoid having to type in your long passphrase every time you use your private key for commits, you need to keep the Pageant program running in the background. You can have it set up to start once per your Windows session so you only have to provide the passphrase once, and then it keeps running in the background. You can do this by typing