var dataGoogleMapsInstalls = [];



var url = 'src/google_cloud_storage.php';
$.ajax({
    url: url
    ,success: function(result){
      // console.log(result);
      var json = csvJSON(result);
      var array = JSON.parse(json);
      array.shift();
      array.map(function(e){
        var item = [];
        item[0] = e['Country'];
        item[1] = parseInt(e['Install events']);

        dataGoogleMapsInstalls.push(item);
        // console.log(e);
      });
      dataGoogleMapsInstalls = dataGoogleMapsInstalls[0];
      console.log(dataGoogleMapsInstalls);
      loadGoogleMap();
    }
    ,error: function(xhr){
      console.error("An error occured: " + xhr.status + " " + xhr.statusText);
    }
})
;

function loadGoogleMap()
{
  google.charts.load('current', {
    'packages':['geochart'],
    // Note: you will need to get a mapsApiKey for your project.
    // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
    'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
  });
  google.charts.setOnLoadCallback(drawRegionsMap);

}

function drawRegionsMap() {
/*  var data = google.visualization.arrayToDataTable([
    ['Country', 'Installs'],
    ['Germany', 200],
    ['United States', 300],
    ['Brazil', 400],
    ['Canada', 500],
    ['France', 600],
    ['RU', 700]
  ]);*/

  var data = google.visualization.arrayToDataTable([
    ['Country', 'Installs'],
    dataGoogleMapsInstalls
  ]);

  var options = {};

  var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

  chart.draw(data, options);
}




function csvJSON(csv){

  var lines=csv.split("\n");

  var result = [];

  var headers=lines[0].split(",");

  for(var i=1;i<lines.length;i++){

      var obj = {};
      var currentline=lines[i].split(",");

      for(var j=0;j<headers.length;j++){
          obj[headers[j]] = currentline[j];
      }

      result.push(obj);

  }

  //return result; //JavaScript object
  return JSON.stringify(result); //JSON
}