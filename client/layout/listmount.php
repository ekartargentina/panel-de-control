<div class="row m-t-25">
    <div class="col-sm-12 col-lg-12">
        <h2 class="pb-2 display-5" id="sourceURL">Radio: Offline</h2>
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c1">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-account-o"></i>
                    </div>
                    <div class="text">
                        <h2 id="listeners">0</h2>
                        <span>listeners online</span>
                    </div>
                </div>
                <div class="overview-chart">
                    <canvas id="widgetChart1"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="overview-item overview-item--c2">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-accounts-outline"></i>
                    </div>
                    <div class="text">
                        <h2 id="max-listeners">0</h2>
                        <span>max listeners online</span>
                    </div>
                </div>
                <div class="overview-chart">
                    <canvas id="widgetChart2"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>