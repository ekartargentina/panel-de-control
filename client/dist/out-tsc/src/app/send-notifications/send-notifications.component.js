import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FirebaseService } from '../firebase.service';
var SendNotificationsComponent = /** @class */ (function () {
    function SendNotificationsComponent(toastr, firebaseService) {
        this.toastr = toastr;
        this.firebaseService = firebaseService;
    }
    SendNotificationsComponent.prototype.ngOnInit = function () {
    };
    SendNotificationsComponent.prototype.send = function () {
        var _this = this;
        var bodyData = {
            "to": "/topics/all",
            "collapse_key": "type_a",
            "notification": {
                "body": this.textMessage,
                "title": this.titleMessage
            },
            "data": {
                "body": this.textMessage,
                "title": this.titleMessage
            }
        };
        this.firebaseService.send(bodyData)
            .subscribe(function (res) {
            console.log(res);
            _this.notificacion();
        });
    };
    SendNotificationsComponent.prototype.notificacion = function () {
        this.toastr.success(this.titleMessage + "\n" + this.textMessage, '¡Notificación enviada!');
    };
    SendNotificationsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-send-notifications',
            templateUrl: './send-notifications.component.html',
            styleUrls: ['./send-notifications.component.sass']
        }),
        tslib_1.__metadata("design:paramtypes", [ToastrService,
            FirebaseService])
    ], SendNotificationsComponent);
    return SendNotificationsComponent;
}());
export { SendNotificationsComponent };
/*

<script type="text/javascript">
$(document).ready(function() {
  $(document).on('click', '#enviarMensaje', function(event) {
    var bodyData = {
                   "to" : "/topics/all",
                   "collapse_key" : "type_a",
                   "notification" : {
                       "body" : $("#textMessage").val(),
                       "title": $("#titleMessage").val()
                   },
                   "data" : {
                       "body" : $("#textMessage").val(),
                       "title": $("#titleMessage").val()
                   }
                  };

    $.ajax({
      url: 'https://fcm.googleapis.com/fcm/send',
      headers: {
        'Authorization':'key=AAAAhygL5Mg:APA91bFU2TekeEdEI8oiDhWalBTtnKvlC2-iYn3Ub-n9bC2IY_A5wGsc2R5ZXianpKq5ucflR9B70W6WtUXWPluLDMdZ57zdpEY081C3DhiIqA7bdVrCf6jwDsJnjJSLmsnRPfPnMWEpf97kGbYcoiGDfK6YUPtowg',
      },
      type: 'POST',
      contentType: 'application/json; charset=utf-8',
      data: JSON.stringify(bodyData),
      dataType: "json",
      cache: false,
      success: function(result) {}
    });
  });
});
</script>
*/ 
//# sourceMappingURL=send-notifications.component.js.map