import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ListenersService } from '../listeners.service';
var ListmountsComponent = /** @class */ (function () {
    function ListmountsComponent(listenersService) {
        this.listenersService = listenersService;
    }
    ListmountsComponent.prototype.ngOnInit = function () {
        this.getStatus();
    };
    ListmountsComponent.prototype.getStatus = function () {
        var _this = this;
        this.listenersService.status()
            .subscribe(function (radios) {
            if (typeof radios.icestats.source !== "undefined") {
                if (typeof radios.icestats.source[0] === "undefined") {
                    _this.listmounts = [radios.icestats.source];
                }
                else {
                    _this.listmounts = radios.icestats.source;
                }
            }
        });
    };
    ListmountsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-listmounts',
            templateUrl: './listmounts.component.html',
            styleUrls: ['./listmounts.component.sass']
        }),
        tslib_1.__metadata("design:paramtypes", [ListenersService])
    ], ListmountsComponent);
    return ListmountsComponent;
}());
export { ListmountsComponent };
//# sourceMappingURL=listmounts.component.js.map