import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
var FirebaseService = /** @class */ (function () {
    function FirebaseService(http) {
        this.http = http;
        this.firebaseUrl = "https://fcm.googleapis.com/fcm/send";
    }
    FirebaseService.prototype.send = function (bodyData) {
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'key=AAAAhygL5Mg:APA91bFU2TekeEdEI8oiDhWalBTtnKvlC2-iYn3Ub-n9bC2IY_A5wGsc2R5ZXianpKq5ucflR9B70W6WtUXWPluLDMdZ57zdpEY081C3DhiIqA7bdVrCf6jwDsJnjJSLmsnRPfPnMWEpf97kGbYcoiGDfK6YUPtowg',
            })
        };
        return this.http.post(this.firebaseUrl, bodyData, httpOptions);
    };
    FirebaseService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], FirebaseService);
    return FirebaseService;
}());
export { FirebaseService };
//# sourceMappingURL=firebase.service.js.map