import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
var MapListenersComponent = /** @class */ (function () {
    function MapListenersComponent() {
    }
    MapListenersComponent.prototype.ngOnInit = function () {
        var latlong = new google.maps.LatLng(35.2271, -80.8431);
        var mapProperties = {
            center: latlong,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        var marker = new google.maps.Marker({ position: latlong, map: this.map });
    };
    tslib_1.__decorate([
        ViewChild('map'),
        tslib_1.__metadata("design:type", Object)
    ], MapListenersComponent.prototype, "mapElement", void 0);
    MapListenersComponent = tslib_1.__decorate([
        Component({
            selector: 'app-map-listeners',
            templateUrl: './map-listeners.component.html',
            styleUrls: ['./map-listeners.component.sass']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], MapListenersComponent);
    return MapListenersComponent;
}());
export { MapListenersComponent };
//# sourceMappingURL=map-listeners.component.js.map