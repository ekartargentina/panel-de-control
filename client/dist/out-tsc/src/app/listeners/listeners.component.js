import * as tslib_1 from "tslib";
import { Component, Renderer2, ElementRef } from '@angular/core';
import { ListenersService } from '../listeners.service';
import { ViewChild } from '@angular/core';
var ListenersComponent = /** @class */ (function () {
    function ListenersComponent(listenersService, renderer) {
        this.listenersService = listenersService;
        this.renderer = renderer;
    }
    ListenersComponent.prototype.ngOnInit = function () {
    };
    ListenersComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            var latlong = new google.maps.LatLng(35.2271, -80.8431);
            var mapProperties = {
                center: latlong,
                zoom: 30,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapProperties);
            _this.getMounts();
        }, 1000);
    };
    ListenersComponent.prototype.getMounts = function () {
        var _this = this;
        this.listenersService.listmounts()
            .subscribe(function (listmounts) {
            _this.mounts = [];
            var sources = listmounts.source;
            if (typeof sources !== 'undefined' && sources.length > 0) {
                for (var _i = 0, sources_1 = sources; _i < sources_1.length; _i++) {
                    var i = sources_1[_i];
                    var mount = i['@attributes'].mount;
                    _this.mounts.push({ 'mount': mount, 'listeners': [] });
                    _this.getListenersMount(mount);
                }
            }
            else {
                var mount = listmounts.source['@attributes'].mount;
                _this.mounts.push({ 'mount': mount, 'listeners': [] });
                _this.getListenersMount(mount);
            }
        });
    };
    ListenersComponent.prototype.getListenersMount = function (mount) {
        var _this = this;
        this.listenersService.get_radio_listeners(mount)
            .subscribe(function (listeners) {
            // console.log(listeners.source);
            // El mount donde se agregan los listeners
            var actualMount = _this.mounts.filter(function (obj) {
                return obj.mount === mount;
            });
            actualMount = actualMount[0];
            _this.table_listeners = [];
            var list_listeners = listeners.source.listener;
            console.log(listeners.source.Listeners);
            if (typeof listeners.source.Listeners !== 'undefined' && parseInt(listeners.source.Listeners) > 0) {
                console.log(list_listeners);
                var ip = null;
                var connected = null;
                var location_1 = null;
                if (Array.isArray(list_listeners)) {
                    for (var _i = 0, list_listeners_1 = list_listeners; _i < list_listeners_1.length; _i++) {
                        var i = list_listeners_1[_i];
                        ip = i.IP;
                        connected = i.Connected;
                        _this.getUbicacionListener(actualMount, ip, connected);
                    }
                }
                else {
                    ip = list_listeners.IP;
                    connected = list_listeners.Connected;
                    _this.getUbicacionListener(actualMount, ip, connected);
                }
                _this.mapFitBounds(actualMount);
            }
        });
    };
    ListenersComponent.prototype.getUbicacionListener = function (actualMount, ip, connected) {
        var _this = this;
        if (!this.isLocationSaved(ip)) {
            this.listenersService.obtenerUbicacion(ip)
                .subscribe(function (res) {
                // Obtener respuesta
                //Armar item para guardar en localStorage
                var location = {
                    'city': res.city,
                    'region': res.region_name,
                    'country': res.country_name,
                    'lat': res.latitude,
                    'long': res.longitude
                };
                var latlong = new google.maps.LatLng(location.lat, location.long);
                var marker = new google.maps.Marker({ position: latlong, map: _this.map });
                // Agrego el listener a la lista
                var listenerObj = { 'ip': ip, 'connected': connected, 'location': location };
                _this.addListenerToMount(listenerObj, actualMount);
                // Guardo el location para esta ip en localStorage
                var key = res.ip;
                localStorage.setItem(key, JSON.stringify(location));
            });
        }
        else {
            var location_2 = this.getLocationByIp(ip);
            var latlong = new google.maps.LatLng(location_2.lat, location_2.long);
            var marker = new google.maps.Marker({ position: latlong, map: this.map });
            var listenerObj = { 'ip': ip, 'connected': connected, 'location': location_2 };
            this.addListenerToMount(listenerObj, actualMount);
        }
    };
    ListenersComponent.prototype.isLocationSaved = function (ip) {
        return this.getLocationByIp(ip) != null;
    };
    ListenersComponent.prototype.getLocationByIp = function (ip) {
        return JSON.parse(localStorage.getItem(ip));
    };
    ListenersComponent.prototype.addListenerToMount = function (listenerObj, actualMount) {
        var ip = listenerObj.ip;
        var connected = listenerObj.connected;
        var location = listenerObj.location;
        actualMount.listeners.push({ 'ip': ip, 'connected': connected, 'location': location });
    };
    ListenersComponent.prototype.mapFitBounds = function (actualMount) {
        var _this = this;
        var _self = this;
        var locations = actualMount.listeners;
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < locations.length; i++) {
            var position = new google.maps.LatLng(locations[i].location.lat, locations[i].location.long);
            console.log(actualMount);
            var marker = new google.maps.Marker({
                position: position,
                map: this.map
            });
            //extend the bounds to include each marker's position
            bounds.extend(position);
        }
        setTimeout(function () {
            //now fit the map to the newly inclusive bounds
            _this.map.fitBounds(bounds);
            var listener = google.maps.event.addListener(_this.map, "idle", function () {
                _self.map.setZoom(5);
                google.maps.event.removeListener(listener);
            });
        }, 5000);
    };
    tslib_1.__decorate([
        ViewChild("mapElement", { read: ElementRef }),
        tslib_1.__metadata("design:type", ElementRef)
    ], ListenersComponent.prototype, "mapElement", void 0);
    ListenersComponent = tslib_1.__decorate([
        Component({
            selector: 'app-listeners',
            templateUrl: './listeners.component.html',
            styleUrls: ['./listeners.component.sass']
        }),
        tslib_1.__metadata("design:paramtypes", [ListenersService,
            Renderer2])
    ], ListenersComponent);
    return ListenersComponent;
}());
export { ListenersComponent };
//# sourceMappingURL=listeners.component.js.map