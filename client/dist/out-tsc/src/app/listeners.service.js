import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
var ListenersService = /** @class */ (function () {
    function ListenersService(http) {
        this.http = http;
    }
    ListenersService.prototype.get_radio_listeners = function (mount) {
        var url = environment.urlAPIRest + "/get_radio_listeners.php?mount=" + mount;
        var user = 'admin';
        var password = 'pepe99';
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'text/xml'
                // 'Authorization': "Basic "+btoa(user+":"+password)
            })
        };
        return this.http.get(url);
    };
    ListenersService.prototype.status = function () {
        var url = "http://35.185.74.33:3389/status-json.xsl";
        return this.http.get(url);
    };
    ListenersService.prototype.listmounts = function () {
        var url = environment.urlAPIRest + "/get_radio_listmounts.php";
        var headers = new HttpHeaders({
        // 'Content-Type':  'text/xml',
        // 'Authorization': 'my-auth-token'
        });
        return this.http.get(url);
    };
    ListenersService.prototype.obtenerUbicacion = function (ip) {
        var url = "http://api.ipstack.com/" + ip + "?access_key=1b61986851b7516d3b8721c49a260e65";
        return this.http.get(url);
    };
    ListenersService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], ListenersService);
    return ListenersService;
}());
export { ListenersService };
//# sourceMappingURL=listeners.service.js.map