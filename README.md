# Readme
Por favor, leer esto antes de comenzar a trabajar.

##¿Cómo empiezo?

Hacer clone del proyecto:

	$ git clone https://grupoargentinoinfo@bitbucket.org/ekartargentina/panel-de-control.git

**Nota: No hacer clone de git clone git@bitbucket.org:ekartargentina/panel-de-control.git**


##¿Cómo trabajo en mi localhost?

Abrir la consola
	
	cmd.exe

Ir a la carpeta de este repositorio git
	
	cd [RUTA-PROYECTO]
	cd client

Instalar node_modules

	npm install

Correr en localhost el angular

	ng serve -o

##¿Cómo compilar el cliente (/client)?

1.Abrir la consola

	cmd.exe

2.Ir a la carpeta de este repositorio git
	
	cd [RUTA-PROYECTO]
	cd client

3.Instalar node_modules
	
	npm install

4.Compilar angular

	ng build --prod

Esto creará una carpeta */dist/controlpanel-webradio*.
